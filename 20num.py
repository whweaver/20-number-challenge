import random
from multiprocessing import Pool, Lock
from time import time

def get_num(existing):
	num = 0
	while num in existing:
		num = random.randint(1, 999)
	return num

def run_game(*args):
	ary = [0] + [None] * 20 + [1000]
	num_numbers = 0

	while True:
		num = get_num(ary)

		low_num = 0
		low_idx = 0
		high_num = 1000
		high_idx = 21
		for idx, existing in enumerate(ary):
			if existing is not None:
				if existing < num:
					low_num = existing
					low_idx = idx
				else:
					high_num = existing
					high_idx = idx
					break

		num_open_indices = high_idx - low_idx - 1

		if num_open_indices <= 0:
			return num_numbers

		new_idx = low_idx + 1 + int(((num - low_num) / (high_num - low_num)) * num_open_indices)
		ary[new_idx] = num
		num_numbers += 1

	raise "Should never get here!"

def result_cb(lock, hg, result):
	lock.acquire()
	hg[result] += 1
	lock.release()

def simulate(num_sims):
	start = time()

	print("Preparing sims...")
	histogram = [0] * 21
	pool = Pool()
	args = [None] * num_sims
	print(f"Elapsed Time: {time() - start}")

	print("Running sims...")
	results = pool.map(run_game, args, int(num_sims / 100))
	print(f"Elapsed Time: {time() - start}")

	print("Tallying results...")
	for result in results:
		histogram[result] += 1
	print(f"Elapsed Time: {time() - start}")

	print("Cleaning up...")
	pool.close()
	pool.join()
	print(f"Elapsed Time: {time() - start}")

	return histogram

if __name__ == '__main__':
	num_sims = 10000000
	for idx, val in enumerate(simulate(num_sims)):
		print(f"{idx}: {round(val / num_sims * 100, 4)}%")